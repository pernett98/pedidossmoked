(function() {
    'use strict';

    angular
        .module('starter.services')
        .factory('factoryProductos', factoryProductos);

    factoryProductos.$inject = ['ServiceProductos'];

    /* @ngInject */
    function factoryProductos(ServiceProductos) {
        var service = {
            getProductos: getProductos,
            getProductoById: getProductoById,
            createProducto: createProducto,
            updateProducto: updateProducto,
            disableProducto: disableProducto,
            counter: 3, //variable temporal para simular los ids
            productos: [{
              id: '01',
              nombre: 'comida 1',
              precio: 5488,
              img: './images/example-images/doughnut.svg',
              isDisabled: false
            }, {
              id: '02',
              nombre: 'comida 2',
              precio: 54888,
              img: './images/example-images/fried-egg.svg',
              isDisabled: false
            }, {
              id: '03',
              nombre: 'comida 3',
              precio: 53584,
              img: './images/example-images/doughnut.svg',
              isDisabled: false
            }]
        };

        return service;

        function getProductos() {
          // service.productos = ServiceProductos.getProductos();
        }

        function getProductoById(idProducto) {
          return service.productos.find(function (producto) {
            return producto.id == idProducto;
          });
        }

        function createProducto(form) {
          form.id = service.counter++;
          form.img = './images/example-images/fried-egg.svg';
          form.isDisabled = false;
          //ServiceProductos.createProducto(form);
          service.productos.push(form);
          // getProductos();
        }

        function updateProducto(form) {
          service.productos.find(function (producto) {
            if (producto.id == form.id) {
              producto = form;
            }
          });
        }

        function disableProducto(idProducto) {
          service.productos.find(function (producto) {
            if (producto.id == idProducto) {
              producto.isDisabled = !producto.isDisabled;
            }
          });
        }

        getProductos();
    }
})();
