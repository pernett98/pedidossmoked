(function() {
    'use strict';

    angular
        .module('starter.services')
        .factory('factoryUsuarios', factoryUsuarios);

    factoryUsuarios.$inject = ['ServiceUsuarios'];

    /* @ngInject */
    function factoryUsuarios(ServiceUsuarios) {
        var service = {
            getUsuarios: getUsuarios,
            getUsuarioById: getUsuarioById,
            createUsuario: createUsuario,
            updateUsuario: updateUsuario,
            disableUsuario: disableUsuario,
            loginUsuario: loginUsuario,

            counter: 3, //variable temporal para simular los ids
            usuarios: [{
              id: 980307,
              nombre: 'Tatiana',
              apellidos: 'Zapata Cano',
              email: 'tatiana_zapata23151@elpoli.edu.co',
              telefono: '2863617',
              img: './images/ionic.png',
              isDisabled: false,
              password: '123'
            }, {
              id: 980604,
              nombre: 'Sebastian',
              apellidos: 'Pernett Plaza',
              email: 'sebastian@elpoli.edu.co',
              telefono: '9475723',
              img: './images/ben.png',
              isDisabled: false,
              password: '123'
            }, {
              id: 980113,
              nombre: 'Iván Andrés',
              apellidos: 'Gutiérrez Agudelo',
              email: 'ivan_gutierrez23151@elpoli.edu.co',
              telefono: '1332351',
              img: './images/perry.png',
              isDisabled: false,
              password: '123'
            }]
        };

        return service;

        function getUsuarios() {
          // ServiceUsuarios.getUsuarios().then(function (result) {
          //   service.usuarios = result.data;
          // }, function (result) {
          //   alert(result.messege);
          // });
        }

        function getUsuarioById(idUsuario) {
          return service.usuarios.find(function (usuario) {
            return usuario.id == idUsuario;
          });
        }

        function createUsuario(form) {
          //form.id = service.counter++;
          form.img = './images/account.svg';
          form.isDisabled = false;
          //ServiceProductos.createProducto(form);
          service.usuarios.push(form);
          // getProductos();
        }

        function disableUsuario(idUsuario) {
          service.usuarios.find(function (usuario) {
            if (usuario.id == idUsuario) {
                usuario.isDisabled = !usuario.isDisabled;
            }
          });
        }

        function updateUsuario(form) {
          service.usuarios.find(function (usuario) {
            if (usuario.id == form.id) {
              usuario = form;
            }
          });
        }

        function loginUsuario(form) {
          var res, email = form.email, password = form.password;

          service.usuarios.find(function (usuario) {
            if (usuario.email == email) {
              if (usuario.password == password) {
                res = true;
                return true;
              }
            }
          });

          return res;
        }

        getUsuarios();
    }
})();
