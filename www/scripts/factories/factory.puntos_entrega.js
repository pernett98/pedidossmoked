(function() {
    'use strict';

    angular
        .module('starter.services')
        .factory('factoryPuntosEntrega', factoryPuntosEntrega);

    // factoryPuntosEntrega.$inject = [''];

    /* @ngInject */
    function factoryPuntosEntrega() {
        var service = {
            getPuntosEntrega: getPuntosEntrega,
            getPuntoEntregaById: getPuntoEntregaById,
            createPuntoEntrega: createPuntoEntrega,
            updatePuntoEntrega: updatePuntoEntrega,
            disablePuntoEntrega: disablePuntoEntrega,
            counter: 3,
            puntosEntrega: [
              {
                id: '01',
                nombre: 'bar 1',
                isDisabled: false
              },
              {
                id: '02',
                nombre: 'cocina 1',
                isDisabled: false
              }
            ]
        };

        return service;

        function getPuntosEntrega() {

        }

        function getPuntoEntregaById(idPuntoEntrega) {
          return service.puntosEntrega.find(function (puntoEntrega) {
            return puntoEntrega.id == idPuntoEntrega;
          });
        }

        function createPuntoEntrega(form) {
          form.id = service.counter++;
          form.isDisabled = false;
          service.puntosEntrega.push(form);
        }

        function updatePuntoEntrega(form) {
          service.puntosEntrega.find(function (puntoEntrega) {
            if (puntoEntrega.id == form.id) {
              puntoEntrega = form;
            }
          });
        }

        function disablePuntoEntrega(idPuntoEntrega) {
          service.puntosEntrega.find(function (PuntoEntrega) {
            if (puntoEntrega.id == idPuntoEntrega) {
              puntoEntrega.isDisabled = !puntoEntrega.isDisabled;
            }
          });
        }

    }
})();
