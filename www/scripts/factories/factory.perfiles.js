(function() {
    'use strict';

    angular
        .module('starter.services')
        .factory('factoryPerfiles', factoryPerfiles);

    // factoryPerfiles.$inject = [''];

    /* @ngInject */
    function factoryPerfiles() {
        var service = {
            getPerfiles: getPerfiles,
            getPerfilById: getPerfilById,
            createPerfil: createPerfil,
            updatePerfil: updatePerfil,
            disablePerfil: disablePerfil,
            counter: 4,
            perfiles: [
              {
                id: '01',
                nombre: 'Administrador',
                isDisabled: false
              },
              {
                id: '02',
                nombre: 'Mesero',
                isDisabled: false
              },
              {
                id: '03',
                nombre: 'Cliente',
                isDisabled: false
              }
            ]
        };

        return service;

        function getPerfiles() {

        }

        function getPerfilById(idPerfil) {
          return service.perfiles.find(function (perfil) {
            return perfil.id == idPerfil;
          });
        }

        function createPerfil(form) {
          form.id = service.counter++;
          form.isDisabled = false;
          service.perfiles.push(form);
        }

        function updatePerfil(form) {
          service.perfiles.find(function (perfil) {
            if (perfil.id == form.id) {
              perfil = form;
            }
          });
        }

        function disablePerfil(idPerfil) {
          service.perfiles.find(function (perfil) {
            if (perfil.id == idPerfil) {
              perfil.isDisabled = !perfil.isDisabled;
            }
          });
        }

    }
})();
