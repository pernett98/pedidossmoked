(function() {
    'use strict';

    angular
        .module('starter.controllers')
        .controller('UsuariosCtrl', UsuariosCtrl);

    UsuariosCtrl.$inject = ['$scope', 'factoryUsuarios', '$state'];

    /* @ngInject */
    function UsuariosCtrl($scope, factoryUsuarios, $state) {
        var vm = this;
        vm.form = {
          id: undefined,
          nombre: "",
          apellidos: "",
          email: "",
          telefono: ""
        };

        factoryUsuarios.getUsuarios();
        vm.factoryUsuarios = factoryUsuarios;
        vm.usuarios = vm.factoryUsuarios.usuarios;
        vm.createUsuario = createUsuario;
        vm.newUsuario = newUsuario;
        vm.disableUsuario = disableUsuario;
        vm.updateUsuario = updateUsuario;
        vm.justLetters = justLetters;

        function createUsuario() {
          // esta funcion dirige la app a la vista del formulario
          $state.go('tab.usuarios-nuevo');
        }

        function newUsuario() {
          factoryUsuarios.createUsuario(vm.form);
          $state.go('tab.usuarios');
        }

        function disableUsuario(idUsuario) {
          factoryUsuarios.disableUsuario(idUsuario);
        }

        if ($state.params.idUsuario) {
          vm.form = factoryUsuarios.getUsuarioById($state.params.idUsuario);
        }

        function updateUsuario() {
          factoryUsuarios.updateUsuario(vm.form);
          $state.go('tab.usuarios', {});
        }

        function justLetters() {
        	if((event.keyCode < 97 || event.keyCode > 122) && (event.keyCode < 65 || event.keyCode > 90)) {
              event.returnValue = false;
          }
        }
    }

})();
