(function() {
  'use strict';

  angular
    .module('starter.controllers')
    .controller('ProductosCtrl', ProductosCtrl);

  ProductosCtrl.$inject = ['$scope', 'factoryProductos', '$state'];

  /* @ngInject */
  function ProductosCtrl($scope, factoryProductos, $state) {
    var vm = this;
    vm.form = {
      nombre: "",
      precio: undefined
    };

    factoryProductos.getProductos();
    vm.factoryProductos = factoryProductos;
    vm.productos = vm.factoryProductos.productos;
    vm.createProducto = createProducto;
    vm.newProducto = newProducto;
    vm.updateProducto = updateProducto;
    vm.disableProducto = disableProducto;

    function createProducto() {
      // esta funcion dirige la app a la vista del formulario
      $state.go('tab.productos-nuevo');
    }

    function newProducto() {
      factoryProductos.createProducto(vm.form);
      $state.go('tab.productos');
    }

    function updateProducto() {
      factoryProductos.updateProducto(vm.form);
      $state.go('tab.productos', {});
    }

    function disableProducto(idProducto) {
      factoryProductos.disableProducto(idProducto);
    }

    if ($state.params.idProducto) {
      vm.form = factoryProductos.getProductoById($state.params.idProducto);
    }
  }
})();
