(function() {
    'use strict';

    angular
        .module('starter.controllers')
        .controller('MesasCtrl', MesasCtrl);

    MesasCtrl.$inject = ['factoryMesas', '$state'];

    /* @ngInject */
    function MesasCtrl(factoryMesas, $state) {
        var vm = this;
        vm.form = {};
        vm.mesas = [];

        factoryMesas.getMesas();
        vm.factoryMesas = factoryMesas;
        vm.mesas = vm.factoryMesas.mesas;
        vm.createMesa = createMesa;
        vm.newMesa = newMesa;
        vm.updateMesa = updateMesa;
        vm.disableMesa = disableMesa;

        function createMesa() {
          $state.go('tab.mesa-nuevo');
        }

        function newMesa() {
          factoryMesas.createMesa(vm.form);
          $state.go('tab.mesas');
        }

        function updateMesa() {
          factoryMesas.updateMesa(vm.form);
          $state.go('tab.mesas', {});
        }

        function disableMesa(idProducto) {
          factoryMesas.disableMesa(idProducto);
        }

        if ($state.params.idMesa) {
          vm.form = factoryMesas.getMesaById($state.params.idMesa);
        }
    }
})();
