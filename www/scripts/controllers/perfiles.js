(function() {
    'use strict';

    angular
        .module('starter.controllers')
        .controller('PerfilesCtrl', PerfilesCtrl);

    PerfilesCtrl.$inject = ['factoryPerfiles', '$state'];

    /* @ngInject */
    function PerfilesCtrl(factoryPerfiles, $state) {
        var vm = this;
        vm.form = {};
        vm.perfiles = [];
        factoryPerfiles.getPerfiles();
        vm.factoryPerfiles = factoryPerfiles;
        vm.perfiles = vm.factoryPerfiles.perfiles;
        vm.createPerfil = createPerfil;
        vm.newPerfil = newPerfil;
        vm.updatePerfil = updatePerfil;
        vm.disablePerfil = disablePerfil;

        function createPerfil() {
          $state.go('tab.perfil-nuevo');
        }

        function newPerfil() {
          factoryPerfiles.createPerfil(vm.form);
          $state.go('tab.perfiles');
        }

        function updatePerfil() {
          factoryPerfiles.updatePerfil(vm.form);
          $state.go('tab.perfiles', {});
        }

        function disablePerfil(idProducto) {
          factoryPerfiles.disablePerfil(idProducto);
        }

        if ($state.params.idPerfil) {
          vm.form = factoryPerfiles.getPerfilById($state.params.idPerfil);
        }
    }
})();
